from flask import Flask, render_template, send_from_directory
from flask_socketio import SocketIO, emit

from collections import OrderedDict
from celery import Celery
from datetime import timedelta

import json

from envirophat import weather, light
import RPi.GPIO as GPIO
import time

global pwm
from Adafruit_PWM_Servo_Driver import PWM

servoPIN = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO 18 als PWM mit 50Hz
p.start(11) # Initialisierung

pwm = PWM(0x40)
pwm.setPWMFreq(1000)

# setup flask app
app = Flask(__name__, static_url_path='')

# setup celery task queue
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'

celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])

app.config['CELERYBEAT_SCHEDULE'] = {
    'update-temperature': {
        'task': 'app.get_temperature',
        'schedule': timedelta(seconds=1)
    },
    'update-humidity': {
        'task': 'app.get_humidity',
        'schedule': timedelta(seconds=1)
    },
    'update-luminosity': {
        'task': 'app.get_luminosity',
        'schedule': timedelta(seconds=1)
    },

}

celery.conf.update(app.config)

# setup websockets
socketio = SocketIO(app, message_queue=app.config['CELERY_BROKER_URL'])

@celery.task
def get_temperature():
    temperature = weather.temperature()
    print(temperature)

    socketio.emit('temperature', temperature)
    return temperature

@celery.task
def get_humidity():
    humidity = 69
    print(humidity)

    socketio.emit('humidity', humidity)
    return humidity

@celery.task
def get_luminosity():
    luminosity = light.light()
    print(luminosity)

    socketio.emit('luminosity', luminosity)
    return luminosity

@socketio.on('water')
def set_water():
    for i in range(1,30):
        p.ChangeDutyCycle(i)
        i =+ 1
        time.sleep(0.5)
    print('received message: water')

@socketio.on('leds')
def set_lights(message):
    print("LEDs\n\n\n")
#    parameters = json.loads(message)
    for value in message:
        channel = int(value)
        brightness = 4095-int(message[value])
        pwm.setPWM(channel, brightness)
    print('received message: ' + str(message))

@app.route('/')
def hello_world():
    get_temperature.apply_async(countdown=10)
    return render_template('index.html')

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

@socketio.on('connect')
def connect():
    emit('my response', {'data': 'Connected'})
    print('Client connected')

@socketio.on('disconnect')
def disconnect():
    print('Client disconnected')

if __name__ == '__main__':
    socketio.run(app, host="0.0.0.0")