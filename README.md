# Grhome home grow box

## Setup
Install everything
```bash
sudo apt-get install redis-server
pip install -r requirements.txt
```

Run the redis queue
``` bash
redis-server
```

Run the celery worker
```bash
celery -A app.celery worker -B --loglevel=info
```

Run the app
```bash
python app.py
```